from django.urls import path
from glassapp import views
from django.views.generic import TemplateView
# from .views import ContactCreateView




app_name = 'glassapp'

urlpatterns = [
    path('home/', views.Home.as_view(), name='home'),
    path('about/', views.About.as_view(), name='about'),
    path('our-glasses/', views.Glasses.as_view(), name='glasses'),
    path('shop/', views.Shop.as_view(), name='shop'),
    path('contact/', views.ContactFormView.as_view(), name='contact'),
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('login/', views.Loginview.as_view(), name='login'),
    path('logout/', views.Logout.as_view(), name='logout'),
    path('detail/<slug:slug>/', views.DetailView.as_view(), name='detail'),

]

