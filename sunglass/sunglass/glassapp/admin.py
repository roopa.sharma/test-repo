from django.contrib import admin
from .models import *






class ImageAdmin(admin.ModelAdmin):
    list_display = ("images", "price", "description")
admin.site.register(Image, ImageAdmin)

class ContactAdmin(admin.ModelAdmin):
    list_display = ("Name", "Phone_Number", "Email", "Message")
admin.site.register(Contact, ContactAdmin)


@admin.register(Organisation)
class OrgAdmin(admin.ModelAdmin):
    list_display = ("name", "city", "created", "modified")

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ("first_name", "last_name", "age")

@admin.register(OrganisationUser)
class OrganisationUserAdmin(admin.ModelAdmin):
    list_display = ("user", "organisation", "role")



# @admin.register(Users)
# class UsersAdmin(admin.ModelAdmin):
#     list_display = ("first_name", "last_name", "created", "modified")

