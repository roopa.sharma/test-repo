from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from django.contrib.auth.base_user import BaseUserManager
from django_extensions.db.models import TimeStampedModel
from django import forms
from django.contrib import admin
from glassapp.roles import ROLES


class CustomUserManager(BaseUserManager):

    def create_user(self, email, password=None, **extra_fields):

        if not email:
            raise ValueError('The Email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):

        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self.create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(max_length=255, unique=True)
    first_name = models.CharField(max_length=50, null=False, blank=False)
    last_name = models.CharField(max_length=20, null=True, blank=True)
    age = models.CharField(max_length=30, null=True, blank=True)
    # organisation = models.ManyToManyField('Organisation')
    # role = models.CharField(max_length=50, null=True, blank=True, choices=ROLES, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']



# def user_directory_path(instance, filename):
#     return 'glassapp_{}/{}'.format(instance, filename)


class Image(models.Model):
    images = models.ImageField(upload_to='media/')
    price = models.CharField(max_length=100)
    description = models.TextField(null=True, max_length=1000)

    class Meta:

        verbose_name = "Pic"
        ordering = ["-images", "description", "price"]

    def __str__(self):
        return self.description


class Contact(models.Model):
    Name = models.CharField(max_length=100, null=True, blank=True)
    Phone_Number = models.CharField(max_length=15, null=True, blank=True)
    Email = models.EmailField(max_length=100, null=True, blank=True)
    Message = models.CharField(max_length=150, null=True, blank=True)

    class Meta:
        verbose_name_plural = "ContactUs"
        verbose_name = "ContactUs"
        ordering = ["Name", "Phone_Number", "Email", "Message"]

    def __str__(self):
        return self.Name


class Organisation(TimeStampedModel):
    name = models.CharField(max_length=30, null=False, blank=False)
    city = models.CharField(max_length=20, null=False, blank=False)

    # users = models.ManyToManyField(
    #     'User.User',
    #     through='OrganisationUser',
    #     related_name='userr',
    #     blank=True
    # )
    # organisation = models.ManyToManyField(
    #     'Organisation',
    #     through='OrganisationUser',
    #     related_name='Org',
    #     blank=True
    # )


    class Meta:
        verbose_name = "Organisation"
        verbose_name_plural = "Organisation"

    def __str__(self):
        return self.name


# class Users(TimeStampedModel):
#
#     first_name = models.CharField(max_length=50, null=False, blank=False)
#     last_name = models.CharField(max_length=20, null=True, blank=True)
#     age = models.CharField(max_length=30, null=True, blank=True)
#     organisation = models.ManyToManyField(Organisation)
#     role = models.CharField(max_length=50, null=True, blank=True, choices=ROLES, unique=True)
#
#     # username = models.CharField(max_length=30, unique=True)
#
#     class Meta:
#             verbose_name = "User"
#             verbose_name_plural = "User"
#
    # def __str__(self):
    #     return self.first_name

class OrganisationUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE)
    role = models.CharField(max_length=50, null=True, blank=True, choices=ROLES, unique=True)

    class Meta:
        verbose_name = "OrgUser"

    def __str__(self):
            return self.user.first_name + ' in ' + self.organisation.name + ' as a ' + self.role

    @property
    def usercount(self):
        return User.objects.filter(organisation__user=self.user).count()























