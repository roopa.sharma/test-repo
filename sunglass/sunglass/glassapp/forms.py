from django import forms
from glassapp import models
from django.forms import ModelForm
from .models import Contact
from django import forms
from django.core import validators


class CreateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = models.User
        fields = '__all__'


class ContactCreateForm(forms.Form):
    Name = forms.CharField(max_length=255)
    Phone_Number = forms.CharField(max_length=255)
    Email = forms.EmailField(max_length=100)
    Message = forms.CharField(max_length=150)

    # class Meta:
    #     model = models.Contact
    #     fields = '__all__'

    # def save(self):
    #     pass


# class ValidationError:
#     pass